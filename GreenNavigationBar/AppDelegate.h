//
//  AppDelegate.h
//  GreenNavigationBar
//
//  Created by Jim Budet on 7/18/14.
//  Copyright (c) 2014 Red Book Connect. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
